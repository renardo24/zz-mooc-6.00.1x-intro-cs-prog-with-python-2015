str1 = 'hello';
str2 = ',';
str3 = 'world';

print(type(str1));
print(str1);

print(type(str1[0]));
print(str1[0]);

print(type(str1[1]));
print(str1[1]);

print(type(str1[-1]));
print(str1[-1]);

print(type(len(str1)));
print(len(str1));

# the following causes error
#print(type(str1[len(str1)]));
#print(str1[len(str1)]);

print(type(str1 + str2 + str3));
print(str1 + str2 + str3);

print(type(str1 + str2 + ' ' + str3));
print(str1 + str2 + ' ' + str3);

print(type(str3 * 3));
print(str3 * 3);

print(type('hello' == str1));
print('hello' == str1);

print(type('HELLO' == str1));
print('HELLO' == str1);

print(type('a' in str3));
print('a' in str3);

str4 = str1 + str3;

print(type('low' in str4));
print('low' in str4);

print(type(str3[1:3]));
print(str3[1:3]);

print(type(str3[:3]));
print(str3[:3]);

print(type(str3[:-1]));
print(str3[:-1]);

print(type(str1[1:]));
print(str1[1:]);

print(type(str4[1:9]));
print(str4[1:9]);

print(type(str4[1:9:2]));
print(str4[1:9:2]);

print(type(str4[::-1]));
print(str4[::-1]);

