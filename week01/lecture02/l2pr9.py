#!/usr/bin/python
# For each of the following expressions, indicate the value that prints out
# when the expression is evaluated. If the evaluation would lead to an error,
# write the word 'error'; if nothing would print out, write the word 'blank'.
# While you could simply type these expressions into an IDLE or Canopy shell,
# we encourage you to answer them directly since this will help reinforce
# your understanding of basic Python expressions.
# PS: If the temperatures seem weird to you, like most of the world, you
# probably use the Celsius system. We Americans still use the crazy Fahrenheit system... 
# http://www.fahrenheittocelsius.com/

if 6 > 7:
    print "Yep"

if 6 > 7:
   print "Yep"
else:
   print "Nope"
 
var = 'Panda'
if var == "panda":
   print "Cute!"
elif var == "Panda":
   print "Regal!"
else:
   print "Ugly..."

temp = '32'
#print(ord('3'));
if temp > 85:
   print "Hot"
elif temp > 62:
   print "Comfortable" 
else:
   print "Cold" 

temp = 120
print(temp);
if temp > 85:
   print "Hot"
elif temp > 100:
   print "REALLY HOT!"
elif temp > 60:
   print "Comfortable" 
else:
   print "Cold"

temp = 50
if temp > 85:
   print "Hot"
elif temp > 100:
   print "REALLY HOT!"
elif temp > 60:
   print "Comfortable"
else:
   print "Cold"
