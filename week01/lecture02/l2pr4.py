print(3 + 5.0);
print(type(3 + 5.0));

print(5/2);
print(type(5/2));

print(5/2 == 5/2.0);
print(type(5/2 == 5/2.0));

print(5/2.0);
print(type(5/2.0));

print(round(2.6));
print(type(round(2.6)));

print(int(2.6));
print(type(int(2.6)));

print(2.0 + 5.0);
print(type(2.0 + 5.0));

print(5*2 == 5.0 * 2.0);
print(type(5*2 == 5.0 * 2.0));
