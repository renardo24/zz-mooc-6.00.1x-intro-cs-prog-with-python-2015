print("a" + "bc");
print(3 * "bc");
# print("3" * "bc"); # causes error
print("abcd"[2]);
print("abcd"[0:2]);
print("abcd"[:2]);
print("abcd"[2:]);
print("abcd"[-1]);
print("abcd"[1:3]);

s = 'Python is Fun!'
print(s[1:5]);
print(s[:5]);
print(s[1:]);
print(s[:]);
print(s[1:12:2]);
print(s[1:12:3]);
print(s[::2]);