balance = 4213;
annualInterestRate = 0.2;
monthlyPaymentRate = 0.04;

numberOfMonths = 12;
minimumMonthlyPayment = 0;
monthlyUnpaidBalance = 0;
monthlyInterestRate = annualInterestRate / float(numberOfMonths);
totalPaid = 0;
for month in range(1, numberOfMonths + 1):
    minimumMonthlyPayment = round(balance * monthlyPaymentRate, 2);
    totalPaid += minimumMonthlyPayment;
    monthlyUnpaidBalance = round(balance - minimumMonthlyPayment, 2);
    balance = round((monthlyUnpaidBalance) + (monthlyInterestRate * monthlyUnpaidBalance), 2);
    print("Month: " + str(month));
    #print("Monthly unpaid balance: " + str(monthlyUnpaidBalance));
    print("Minimum monthly payment: " + str(minimumMonthlyPayment));
    print("Remaining balance: " + str(balance));
print("Total paid: " + str(totalPaid));
print("Remaining balance: " + str(balance));