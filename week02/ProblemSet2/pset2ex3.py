balance = 999999;
annualInterestRate = 0.18;
numberOfMonths = 12;
monthlyInterestRate = annualInterestRate / numberOfMonths;

minimum = balance / numberOfMonths;
maximum = (balance * (1 + monthlyInterestRate) ** numberOfMonths) / numberOfMonths;

guessMinimum = (minimum + maximum) / 2;

precision = 0.01;

remain = balance; # if nothing has been paid, then what remains to be paid is the original balance!

while (remain >= precision):
    
    guessMinimum = (minimum + maximum) / 2;
    
    for month in range(1, numberOfMonths + 1):
        newBalance = remain - guessMinimum;
        monthInterest = annualInterestRate / numberOfMonths * newBalance;
        remain = newBalance + monthInterest;
        #print ("Remain: " + str(remain));
    if (remain < 0):
        # we are paying too much so we need to decrease the minimum we will pay.
        # this means that the maximum we chose before now becomes the new guessMinimum
        maximum = guessMinimum;
        remain = balance; # reset the balance to start again
    elif (remain > precision):
        # we are paying too little so we need to increase the minimum we are paying
        # this means that the minimum we chose before now becomes the new guessMinimum
        minimum = guessMinimum;
        remain = balance; # reset the balance to start again
    else:
        # remain is 0 so we have found a good solution
        break;

print("Lowest Payment: %.2f" % guessMinimum);


#Other solution:
#originalBalance = 320000
#annualInterestRate = 0.2
#monthly_interest = annualInterestRate / 12
#low = originalBalance/12
#high = (originalBalance*(1 + monthly_interest)**12)/12
#epsilon = 0.01
#min_payment = (high + low)/2.0
#
#while min_payment*12 - originalBalance >= epsilon:
#    for month in range(0, 12):
#        balance = (originalBalance - min_payment)/10 * (1+monthly_interest)
#
#    if balance < 0:
#        low = min_payment
#    elif balance > 0:
#        high = min_payment
#        min_payment = (high + low)/2.0
#print "lowest payment: " + str(balance)

#
# Problem 3: Using Bisection Search to Make the Program Faster
#(25 points possible)
#
#You'll notice that in Problem 2, your monthly payment had to be a multiple of $10. Why did we make it that way? You can try running your code locally so that the payment can be any dollar and cent amount (in other words, the monthly payment is a multiple of $0.01). Does your code still work? It should, but you may notice that your code runs more slowly, especially in cases with very large balances and interest rates. (Note: when your code is running on our servers, there are limits on the amount of computing time each submission is allowed, so your observations from running this experiment on the grading system might be limited to an error message complaining about too much time taken.)
#
#Well then, how can we calculate a more accurate fixed monthly payment than we did in Problem 2 without running into the problem of slow code? We can make this program run faster using a technique introduced in lecture - bisection search!
#
#The following variables contain values as described below:
#
#    balance - the outstanding balance on the credit card
#
#    annualInterestRate - annual interest rate as a decimal
#
#To recap the problem: we are searching for the smallest monthly payment such that we can pay off the entire balance within a year. What is a reasonable lower bound for this payment value? $0 is the obvious anwer, but you can do better than that. If there was no interest, the debt can be paid off by monthly payments of one-twelfth of the original balance, so we must pay at least this much every month. One-twelfth of the original balance is a good lower bound.
#
#What is a good upper bound? Imagine that instead of paying monthly, we paid off the entire balance at the end of the year. What we ultimately pay must be greater than what we would've paid in monthly installments, because the interest was compounded on the balance we didn't pay off each month. So a good upper bound for the monthly payment would be one-twelfth of the balance, after having its interest compounded monthly for an entire year.
#
#In short:
#
#    Monthly interest rate = (Annual interest rate) / 12.0
#    Monthly payment lower bound = Balance / 12
#    Monthly payment upper bound = (Balance x (1 + Monthly interest rate)12) / 12.0
#
#Write a program that uses these bounds and bisection search (for more info check out the Wikipedia page on bisection search) to find the smallest monthly payment to the cent (no more multiples of $10) such that we can pay off the debt within a year. Try it out with large inputs, and notice how fast it is (try the same large inputs in your solution to Problem 2 to compare!). Produce the same return value as you did in Problem 2.
#
#Note that if you do not use bisection search, your code will not run - your code only has 30 seconds to run on our servers.
#
#
#Test Cases to Test Your Code With. Be sure to test these on your own machine - and that you get the same output! - before running your code on this webpage!
#Click to See Problem 3 Test Cases
#
#Note: The automated tests are leinient - if your answers are off by a few cents in either direction, your code is OK.
#
#Be sure to test these on your own machine - and that you get the same output! - before running your code on this webpage!
#
#Test Cases:
#
#                      
#    	      Test Case 1:
#    	      balance = 320000
#    	      annualInterestRate = 0.2
#
#    	      Result Your Code Should Generate:
#    	      -------------------
#    	      Lowest Payment: 29157.09
#          
#                    
#
#                      
#    	      Test Case 2:
#    	      balance = 999999
#    	      annualInterestRate = 0.18
#    	      
#    	      Result Your Code Should Generate:
#    	      -------------------
#    	      Lowest Payment: 90325.03
#    	  
#                    
#
#
#The code you paste into the following box should not specify the values for the variables balance or annualInterestRate - our test code will define those values before testing your submission. 
#
# Note:
#
#Depending on where, and how frequently, you round during this function, your answers may be off a few cents in either direction. Try rounding as few times as possible in order to increase the accuracy of your result.
#
#Reminder: Only hit "Check" once per submission. We are unable to give you more than 30 checks.
