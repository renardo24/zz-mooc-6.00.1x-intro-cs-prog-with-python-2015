#!/usr/bin/python
# Alphabetical Substrings
# Assume s is a string of lower case characters.
# Write a program that prints the longest substring of s in which the letters occur
# in alphabetical order. For example, if s = 'azcbobobegghakl', then your program should print
# Longest substring in alphabetical order is: beggh
# In the case of ties, print the first substring. For example, if s = 'abcbcd', then your program should print
# Longest substring in alphabetical order is: abc
# For problems such as these, do not include raw_input statements or define the variable s
# in any way. Our automated testing will provide a value of s for you - so the code you
# submit in the following box should assume s is already defined. If you are confused by
# this instruction, please review L4 Problems 10 and 11 before you begin this problem set.

# Note: This problem is fairly challenging. We encourage you to work smart. If you've spent more
# than a few hours on this problem, we suggest that you move on to a different part of the course.
# If you have time, come back to this problem after you've had a break and cleared your head.
s = 'azcbobobegghakl';
#s = 'abcdefghijklmnopqrstuvwxyz'
#s = 'abcbcd';
prevChar = 'a';
longestSubString = '';
tempSubString = '';
for char in s:
    #print('');
    #print("prev:" + prevChar);
    #print("char: " + char);
    if prevChar <= char:
        tempSubString += char;
        #print("temp: " + tempSubString);
    else:
        #print(len(tempSubString));
        #print(len(longestSubString));
        if (len(tempSubString) > len(longestSubString)):
            longestSubString = tempSubString;
            #print("LONGEST: " + longestSubString);
        tempSubString = char;
    prevChar = char;
if (len(longestSubString) == 0 and len(tempSubString) != 0):
    longestSubString = tempSubString
print("Result: " + longestSubString);