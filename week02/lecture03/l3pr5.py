#!/usr/bin/python
# L3 Problem 5a
# In this problem you'll be given a chance to practice writing some for loops.
# 1. Convert the following code into code that uses a for loop.
#print 2
#print 4
#print 6
#print 8
#print 10
#print "Goodbye!"
for num in range (2, 12, 2):
    print(num);
print("Goodbye");

# L3 Problem 5b
# 2. Convert the following code into code that uses a for loop.
#print "Hello!"
#print 10
#print 8
#print 6
#print 4
#print 2
print("Hello!");
for num in range(10, 0, -2):
    print(num);

# L3 Problem 5c
# 3. Write a for loop that sums the values 1 through end, inclusive. end is a
# variable that we define for you. So, for example, if we define end to be 6,
# your code should print out the result:
# 21
# which is 1 + 2 + 3 + 4 + 5 + 6.
end = 6;
counter = 0;
for num in range(end + 1):
    counter += num;
print(counter);

# For problems such as these, do not include raw_input statements or define the
# variable end. Our automating testing will provide a value of end for you - so
# write your code in the following box assuming end is already defined.
