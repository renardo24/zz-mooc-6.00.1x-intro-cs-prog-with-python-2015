#!/usr/bin/python

# L3 Problem 2a
# In this problem you'll be given a chance to practice writing some while loops.
# 1. Convert the following into code that uses a while loop.
#print 2
#print 4
#print 6
#print 8
#print 10
#print Goodbye!
a = 0;
while (a < 10):
    a += 2;
    print(a);
print("Goodbye!");

# L3 Problem 2b
# 2. Convert the following into code that uses a while loop.
#print Hello!
#print 10
#print 8
#print 6
#print 4
#print 2
print("Hello!");
a = 10;
while (a > 0):
    print(a);
    a -= 2;

# L3 Problem 2c
# 3. Write a while loop that sums the values 1 through end, inclusive. end is a
# variable that we define for you. So, for example, if we define end to be 6,
# your code should print out the result:
# 21
# which is 1 + 2 + 3 + 4 + 5 + 6.
end = 6
start = 1;
counter = 0;
result = 0;
while (counter < end):
    counter += 1;
    result += counter;
print(result);    


# For problems such as these, do not include raw_input statements or define 
# the variable end. Our automating testing will provide a value of end for you -
# so write your code in the following box assuming end is already defined.
