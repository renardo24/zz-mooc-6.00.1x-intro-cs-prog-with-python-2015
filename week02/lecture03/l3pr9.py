#!/usr/bin/python
#L3 Problem 9
# In this problem, you'll create a program that guesses a secret number!
# The program works as follows: you (the user) thinks of an integer between 0 (inclusive) and 100 (not inclusive). The computer makes guesses, and you give it input - is its guess too high or too low? Using bisection search, the computer will guess the user's secret number!
# Here is a transcript of an example session:
#
# Please think of a number between 0 and 100!
# Is your secret number 50?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. l
# Is your secret number 75?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. l
# Is your secret number 87?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. h
# Is your secret number 81?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. l
# Is your secret number 84?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. h
# Is your secret number 82?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. l
# Is your secret number 83?
# Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. c
# Game over. Your secret number was: 83
#
# Your program should use bisection search. So think carefully what that means. What will the
# first guess always be? How should you calculate subsequent guesses? 
#
# Your initial endpoints should be 0 and 100. Do not optimize your subsequent endpoints by making
# them be the halfway point plus or minus 1. Rather, just make them be the halfway point.
#

start = 0;
end = 100;
guess = (end - start) / 2;
solution = 0;
userSelection = '';
prompt = "Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ";

print("Please think of a number between " + str(start) + " and " + str(end) + "!");

while(True):
    print("Is your secret number " + str(guess) + "?");
    userSelection = raw_input(prompt);
    if (userSelection == 'h'):
        end = guess;
        guess = start + ((end - start) / 2);
        #print(guess);
    elif (userSelection == 'l'):
        start = guess;
        guess = start + ((end - start) / 2);
        #print(guess);
    elif (userSelection == 'c'):
        solution = guess;
        break;
    else:
        print("Sorry, I did not understand your input.");
    
print("Game over. Your secret number was: " + str(solution));
