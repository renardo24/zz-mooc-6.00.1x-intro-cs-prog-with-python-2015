def sort3(lst):
    out = []
    for iteration in range(0,len(lst)):
        new = lst[iteration]
        inserted = False
        for j in range(len(out)):
            if new < out[j]:
                out.insert(j, new)
                inserted = True
                break
        if not inserted:
            out.append(new)

        L = out[:]  # the next 3 questions assume this line just executed
        print(L);
        print(iteration);
    return out

lst = [1,4,7,20,3,6,8,10,30,2,0];
print(str(lst) + "<<<");
sortedLst = sort3(lst);
print(sortedLst);