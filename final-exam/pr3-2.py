def sort2(lst):
    for iteration in range(len(lst)):
        minIndex = iteration
        minValue = lst[iteration]
        for j in range(iteration+1, len(lst)):
            if lst[j] < minValue:
                minIndex = j
                minValue = lst[j]
        temp = lst[iteration]
        lst[iteration] = minValue
        lst[minIndex] = temp

        #L = lst[:]  # the next 3 questions assume this line just executed
        #print(L);
        #print(iteration);
    return lst

lst = [1,4,7,20,3,6,8,10,30,2,0];
print(str(lst) + "<<<");
sortedLst = sort2(lst);
print(sortedLst);