# -*- coding: utf-8 -*-
class Frob(object):
    def __init__(self, name):
        self.name = name
        self.before = None
        self.after = None
    def setBefore(self, before):
        # example: a.setBefore(b) sets b before a
        self.before = before
    def setAfter(self, after):
        # example: a.setAfter(b) sets b after a
        self.after = after
    def getBefore(self):
        return self.before
    def getAfter(self):
        return self.after
    def myName(self):
        return self.name
    def __str__(self):
        result = self.name;
        current = self;
        while(current.getBefore() != None):
            current = current.getBefore();
            result = current.myName() + " <-- " + result;
        current = self;
        while(current.getAfter() != None):
            current = current.getAfter();
            result = result + " --> " + current.myName();
        return result;

def insert(atMe, newFrob):
    """
    atMe: a Frob that is part of a doubly linked list
    newFrob:  a Frob with no links 
    This procedure appropriately inserts newFrob into the linked list that atMe is a part of.    
    """

    def insertBefore(atMe, newFrob):
        if (atMe.getBefore() == None):
            atMe.setBefore(newFrob);
            newFrob.setAfter(atMe);
        elif (atMe.getBefore().myName() < newFrob.myName()):
            #print(atMe.getBefore().myName()); # andrew
            #print(newFrob.myName()); # bob
            newFrob.setAfter(atMe);
            newFrob.setBefore(atMe.getBefore());
            atMe.getBefore().setAfter(newFrob);
            atMe.setBefore(newFrob);
        elif (atMe.getBefore().myName() == newFrob.myName()):
            newFrob.setAfter(atMe);
            newFrob.setBefore(atMe.getBefore());
            atMe.getBefore().setAfter(newFrob);
            atMe.setBefore(newFrob);
        else: # (atMe.getBefore().myName() > newFrob.myName()):
            #print(atMe.getBefore().myName()); # don
            #print(newFrob.myName()); # bob
            insertBefore(atMe.getBefore(), newFrob);
    
    def insertAfter(atMe, newFrob):
        if (atMe.getAfter() == None):
            atMe.setAfter(newFrob);
            newFrob.setBefore(atMe);
        elif (atMe.getAfter().myName() > newFrob.myName()):
            #print(atMe.getAfter().myName()); # ruth
            #print(newFrob.myName()); # fred
            newFrob.setBefore(atMe);
            newFrob.setAfter(atMe.getAfter());
            atMe.getAfter().setBefore(newFrob);
            atMe.setAfter(newFrob);
        elif (atMe.getAfter().myName() == newFrob.myName()):
            newFrob.setBefore(atMe);
            newFrob.setAfter(atMe.getAfter());
            atMe.getAfter().setBefore(newFrob);
            atMe.setAfter(newFrob);
        else: #(atMe.getAfter().myName < newFrob.myName()):
            insertAfter(atMe.getAfter(), newFrob);
    
    atMeName = atMe.myName();
    newFrobName = newFrob.myName();
    #print("NFN: " + newFrobName);
    #print("AMFN: " + atMeName);
    if (newFrobName < atMeName):
        insertBefore(atMe, newFrob);
    elif(newFrobName > atMeName):
        insertAfter(atMe, newFrob);
    else:
        insertAfter(atMe, newFrob);

def findFront(start):
    """
    start: a Frob that is part of a doubly linked list
    returns: the Frob at the beginning of the linked list 
    """
    previous = start.getBefore();
    if (previous != None):
        return findFront(previous);
    else:
        return start;
                        
    
eric = Frob('eric');
andrew = Frob('andrew');
ruth = Frob('ruth');
don = Frob('don');
fred = Frob('fred');
martha = Frob('martha');
bob = Frob('bob');
steve = Frob('steve');

insert(eric, andrew)
insert(eric, ruth)
insert(eric, don)
insert(eric, bob)
insert(eric, fred)
insert(eric, steve);
insert(ruth, martha)
insert(eric, Frob("don"));
insert(eric, Frob("ruth"));
insert(eric, Frob('alex'));
insert(eric, Frob('alec'));

print(eric);

front = findFront(eric);
print(front.myName());
