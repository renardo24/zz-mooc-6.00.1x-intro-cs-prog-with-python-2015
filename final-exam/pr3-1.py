def sort1(lst):
    swapFlag = True
    iteration = 0
    while swapFlag:
        swapFlag = False
        for i in range(len(lst)-1):
            if lst[i] > lst[i+1]:
                #print(lst[i]);
                #print(lst[i+1]);
                temp = lst[i+1]
                lst[i+1] = lst[i]
                lst[i] = temp
                swapFlag = True

        #L = lst[:]  # the next 3 questions assume this line just executed
        #print(L);
        #print(iteration);
        iteration += 1
    return lst

lst = [1,4,7,20,3,6,8,10,30,2,0];
print(str(lst) + "<<<");
sortedLst = sort1(lst);
print(sortedLst);