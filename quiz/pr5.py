#PROBLEM 5  (10 points possible)
#Suppose you are given two strings (they may be empty), s1 and s2. You would like to "lace" these strings together, by successively alternating elements of each string (starting with the first character of s1). If one string is longer than the other, then the remaining elements of the longer string should simply be added at the end of the new string. For example, if we lace 'abcd' and 'efghi', we would get the new string: 'aebfcgdhi'.
#
#Write an iterative procedure, called laceStrings(s1, s2) that does this.
#
#Note: You will only get ten checks. Use these judiciously.
def laceStrings(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length, 
    then the extra elements should appear at the end.
    """
    if (not s1):
        return s2;
    if (not s2):
        return s1;
    lens1 = len(s1);
    lens2 = len(s2);
    result = '';
    if (lens1 == lens2):
        for index in range(lens1):
            result += s1[index];
            result += s2[index];
    elif (lens1 > lens2):
        for index in range(lens2):
            result += s1[index];
            result += s2[index];
        result += s1[index+1:];
    else:
        for index in range(lens1):
            result += s1[index];
            result += s2[index];
        result += s2[index+1:];        
    return result;

print(laceStrings(None, "Olivier"));
print(laceStrings("", "Olivier"));
print(laceStrings("Olivier", None));
print(laceStrings("Olivier", ""));

print(laceStrings("       ", "Olivier"));
print(laceStrings("Olivier", "       "));
print(laceStrings("Olivier", "Belgium"));
print(laceStrings('abcde', 'fghij'));

print(laceStrings("Olivier", "Renard"));
print(laceStrings("OlivierRenard", "Belgique"));
print(laceStrings("abcdefg", "yz"));

print(laceStrings('abcd', 'efghi'));
print(laceStrings("ab", "stuvwxyz"));
