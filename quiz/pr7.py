def McNuggets(n):
    """
    n is an int

    Returns True if some integer combination of 6, 9 and 20 equals n
    Otherwise returns False.
    """
    if (n < 0):
        return False;
    elif (n == 0):
        return True;
    elif (McNuggets(n - 20) or McNuggets(n - 9) or McNuggets(n - 6)):
        return True;
    else:
        return False;

print(McNuggets(-10));
print(McNuggets(0));
print(McNuggets(18));
print(McNuggets(27));
print(McNuggets(260));
print(McNuggets(24));
print(McNuggets(26));
print(McNuggets(46));
print(McNuggets(15));
print(McNuggets(16));