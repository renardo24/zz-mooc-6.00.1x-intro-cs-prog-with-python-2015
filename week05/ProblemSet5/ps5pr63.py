#If we make a small change to the line for j in range(i+1, len(L)): such that the code becomes:
#
#      
#def modSwapSort(L): 
#    """ L is a list on integers """
#    print "Original L: ", L
#    for i in range(len(L)):
#        for j in range(len(L)):
#            if L[j] < L[i]:
#                # the next line is a short 
#                # form for swap L[i] and L[j]
#                L[j], L[i] = L[i], L[j] 
#                print L
#    print "Final L: ", L
#      
#    
#What happens to the behavior of swapSort with this new code?
#
#
#No change
#modSwapSort now orders the list in descending order for all lists.
#modSwapSort now orders the list in descending order for SOME lists but not all
#modSwapSort enters an infinite loop.
def modSwapSort(L): 
    """ L is a list on integers """
    print "Original L: ", L
    for i in range(len(L)):
        for j in range(len(L)):
            if L[j] < L[i]:
                # the next line is a short 
                # form for swap L[i] and L[j]
                L[j], L[i] = L[i], L[j] 
                print L
    print "Final L: ", L

#L = [1,4,7,3,8,1,2,3,9,10];
L = [10,3,5,3,2,9,53,89,101,56,3]
print(modSwapSort(L));