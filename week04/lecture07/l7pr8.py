#L7 PROBLEM 8  (5 points possible)
#Consider the following function definition:
#
#def f(n):
#   """
#   n: integer, n >= 0.
#   """
#   if n == 0:
#      return n
#   else:
#      return n * f(n-1)
#When we call f(3) we expect the result 6, but we get 0.
#
#When we call f(1) we expect the result 1, but we get 0.
#
#When we call f(0) we expect the result 1, but we get 0.

def f(n):
   """
   n: integer, n >= 0.
   """
#   print(n);
   if n == 0:
      return 1;
   else:
      return n * f(n-1)

print(f(3));
print(f(1));
print(f(0));
