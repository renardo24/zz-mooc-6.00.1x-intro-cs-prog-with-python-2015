#L7 PROBLEM 7  (5/5 points)
#Consider the following function definition:
#
#def rem(x, a):
#    """
#    x: a non-negative integer argument
#    a: a positive integer argument
#
#    returns: integer, the remainder when x is divided by a.
#    """
#    if x == a:
#        return 0
#    elif x < a:
#        return x
#    else:
#        rem(x-a, a)
#When we call
#
#rem(2, 5)
#IDLE returns 2. When we call
#
#rem(5, 5)
#IDLE returns 0. But when we call
#
#rem(7, 5)
#IDLE does not return anything! 

def rem(x, a):
    """
    x: a non-negative integer argument
    a: a positive integer argument

    returns: integer, the remainder when x is divided by a.
    """
    if x == a:
        return 0
    elif x < a:
        return x
    else:
        return rem(x-a, a)

print rem(2, 5);
print rem(5, 5);
print rem(7, 5);