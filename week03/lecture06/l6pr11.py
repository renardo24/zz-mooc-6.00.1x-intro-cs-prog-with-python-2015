#L6 PROBLEM 11  (5 points possible)
#Consider the following sequence of expressions:
#
#animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}
#
#animals['d'] = ['donkey']
#animals['d'].append('dog')
#animals['d'].append('dingo')
#We want to write some simple procedures that work on dictionaries to return information.
#
#This time, write a procedure, called biggest, which returns the key corresponding to the entry with the largest number of values associated with it. If there is more than one such entry, return any one of the matching keys.
#
#Example usage:
#
#>>> biggest(animals)
#'d'
#If there are no values in the dictionary, biggest should return None.

animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}

animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')

#print(animals);

def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: The key with the largest number of values associated with it
    '''
    if (len(aDict) == 0):
        return None;
    biggestKey = None;
    largestNumber = 0;
    keys = aDict.keys();
    for key in keys:
        if (biggestKey == None):
            biggestKey = key;
        value = aDict[key];
        numberOfValues = len(value);
        if (numberOfValues > largestNumber):
            largestNumber = numberOfValues;
            biggestKey = key;
    return biggestKey;

print(biggest({'C': []}));
print(biggest({}));
print(biggest(animals));