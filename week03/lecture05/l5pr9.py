#L5 PROBLEM 9  (5 points possible)
#A semordnilap is a word or a phrase that spells a different word when backwards ("semordnilap" is a semordnilap of "palindromes"). Here are some examples:
#
#nametag / gateman
#dog / god
#live / evil
#desserts / stressed
#Write a recursive program, semordnilap, that takes in two words and says if they are semordnilap.
#
#This recursive function is not entirely straightforward. There are a few things that you need to check the first time you look at the inputs that you should not check on subsequent recursive calls: you need to make sure that the strings are not single characters, and also you need to be sure that the strings are not equal. If you do this check every time you call your function, though, this will end up interfering with the recursive base case (which we don't want!).
#
#There's a few different ways you can perform checks on the inputs the first time. The first way would be to use keyword arguments. The second way would be to use a global variable, which you'll see in the next lecture video; however, using global variables is always a bit risky and thus not the best way to do this.
#
#The third way to perform checks on the inputs the first time you see them, but not any subsequent time, is to use a wrapper function. This wrapper function performs some checks, then makes a call to the recursive function.
#
#The idea of a wrapper function is really important. You'll see more wrapper functions later. To introduce you to the idea, we are providing you with the wrapper function; your job is to write the recursive function semordnilap that the wrapper function calls. Here is the wrapper function:
#
#def semordnilapWrapper(str1, str2):
#    # A single-length string cannot be semordnilap
#    if len(str1) == 1 or len(str2) == 1:
#        return False
#
#    # Equal strings cannot be semordnilap
#    if str1 == str2:
#        return False
#
#    return semordnilap(str1, str2)
#Fill in the definition for semordnilap in the box below.
# 
#unansweredUnanswered
#Note: In programming there are many ways to solve a problem. For your code to check correctly here, though, you must write your recursive function such that you make a recursive call directly to the function semordnilap. Thank you for understanding.
#
#Hints
#Unequal Strings
#What happens when two strings are not of equal length? What can you immediately return?
#What should your base case be?
#What happens when the strings are of length 1?
#What should your recursive case be?
#Go back to the definition of a semordnilap. What can you say about the first letter of str1 and the last letter of str2?

def semordnilapWrapper(str1, str2):
    # A single-length string cannot be semordnilap
    if len(str1) == 1 or len(str2) == 1:
        return False

    # Equal strings cannot be semordnilap
    if str1 == str2:
        return False

    return semordnilap(str1, str2)

def semordnilap(str1, str2):
    '''
    str1: a string
    str2: a string
    
    returns: True if str1 and str2 are semordnilap;
             False otherwise.
    '''
    print('-' + str1 + '-' + str2 + '-');
    print("0");
    print(str1[0]);
    print(str2[-1]);
    if (len(str1) == 0 or len(str2) == 0):
        return False;
    if (len(str1) != len(str2)):
        return False;
    if (len(str1) == 1 and len(str2) == 1):
        print("1");
        print(str1);
        print(str2);
        if (str1 == str2):
            return True;
        else:
            return False;
    if (str1[0] == str2[-1]):
        print("2");
        print(str1[1:]);
        print(str2[:-1]);
        return semordnilap(str1[1:], str2[:-1]);

print(semordnilapWrapper('god', 'dog'));