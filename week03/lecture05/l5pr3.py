#!/bin/python
# L5 PROBLEM 3  (5 points possible)
# The function recurPower(base, exp) from Problem 2 computed baseexp by decomposing the problem into one recursive case and one base case:
# 
# baseexpbaseexp==base⋅baseexp−11ifexp>0ifexp=0
# Another way to solve this problem just using multiplication (and remainder) is to note that
# 
# baseexpbaseexpbaseexp===(base2)exp2base⋅baseexp−11ifexp>0andexpisevenifexp>0andexpisoddifexp=0
# 
# Write a procedure recurPowerNew which recursively computes exponentials using this idea.
# 
# Note: In programming there are many ways to solve a problem. For your code to check correctly here, though, you must write your recursive function such that you make a recursive call directly to the function recurPowerNew. Thank you for understanding.
# 
def recurPowerNew(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float; base^exp
    '''
    if (exp == 0):
        return 1;
    if (exp > 0 and (exp % 2 != 0)):  # exp is odd
        return base * (recurPowerNew(base, (exp - 1)));
    if (exp > 0 and (exp % 2 == 0)):  # exp is even
        #return (base * base) ** (exp / 2)
        return recurPowerNew((base * base), exp / 2);

