# L5 Problem 7
#(5 points possible)
#
#For this problem, write a recursive function, lenRecur, which computes the length of an input argument (a string), by counting up the number of characters in the string.
#
#Hint: String slicing may be useful in this problem... (http://www.greenteapress.com/thinkpython/html/thinkpython009.html#toc89)
#Note: In programming there are many ways to solve a problem. For your code to check correctly here, though, you must write your recursive function such that you make a recursive call directly to the function lenRecur. Thank you for understanding.
#
#Hint: How would you check if a string is empty without using len()? An easy way you can check if a string, s, is empty is to check the condition,
#
#  s == '' 
def lenRecur(aStr):
    '''
    aStr: a string
    
    returns: int, the length of aStr
    '''
    if (aStr == ''):
        return 0;
    else:
        return 1 + lenRecur(aStr[1:]);

print(lenRecur("London"));

