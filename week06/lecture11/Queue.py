class Queue(object):

    def __init__(self):
        self.vals = [];

    def insert(self, el):
        self.vals.append(el);

    def remove(self):
        if (len(self.vals) == 0):
            raise ValueError();
        else:
            returnValue = self.vals[0];
            del(self.vals[0]);
            return returnValue;
            
