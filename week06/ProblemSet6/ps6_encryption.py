# 6.00x Problem Set 6
#
# Part 1 - HAIL CAESAR!

import string
import random

#WORDLIST_FILENAME = "/home/olivier/omr/projects/course-201501-6.00.1x-intro-cs-prog-with-python/week06/ProblemSet6/words.txt"
ROOT = "/home/orenard/omr/projects/course-201501-6.00.1x-intro-cs-prog-with-python/week06/ProblemSet6/";
WORDLIST_FILENAME = ROOT + "words.txt"
STORY_FILENAME = ROOT + "story.txt";

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)
def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print "Loading word list from file..."
    inFile = open(WORDLIST_FILENAME, 'r')
    wordList = inFile.read().split()
    print "  ", len(wordList), "words loaded."
    return wordList

def isWord(wordList, word):
    """
    Determines if word is a valid word.

    wordList: list of words in the dictionary.
    word: a possible word.
    returns True if word is in wordList.

    Example:
    >>> isWord(wordList, 'bat') returns
    True
    >>> isWord(wordList, 'asdf') returns
    False
    """
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\\:;'<>?,./\"")
    return word in wordList

def randomWord(wordList):
    """
    Returns a random word.

    wordList: list of words  
    returns: a word from wordList at random
    """
    return random.choice(wordList)

def randomString(wordList, n):
    """
    Returns a string containing n random words from wordList

    wordList: list of words
    returns: a string of random words separated by spaces.
    """
    return " ".join([randomWord(wordList) for _ in range(n)])


def randomScrambled(wordList, n):
    """
    Generates a test string by generating an n-word random string
    and encrypting it with a sequence of random shifts.

    wordList: list of words
    n: number of random words to generate and scamble
    returns: a scrambled string of n random words

    NOTE:
    This function will ONLY work once you have completed your
    implementation of applyShifts!
    """
    s = randomString(wordList, n) + " "
    shifts = [(i, random.randint(0, 25)) for i in range(len(s)) if s[i-1] == ' ']
    return applyShift(s, shifts)[:-1]


def getStoryString():
    """
    Returns a story in encrypted text.
    """
    return open(STORY_FILENAME, "r").read()


# (end of helper code)
# -----------------------------------


#
# Problem 1: Encryption
#
def buildCoder(shift):
    """
    Returns a dict that can apply a Caesar cipher to a letter.
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation, numbers and spaces.

    shift: 0 <= int < 26
    returns: dict
    """
    #return "Not yet implemented." # Remove this comment when you code the function
    dict = {};
    if (shift >= 0 and shift < 26):
        for char in string.uppercase:
            num = ord(char);
            num = num + shift;
            if (num > ord(string.uppercase[-1])):
                num = num - 26;
            result = chr(num);
            dict[char] = result
        for char in string.lowercase:
            num = ord(char);
            num = num + shift;
            if (num > ord(string.lowercase[-1])):
                num = num - 26;
            result = chr(num);
            dict[char] = result
    return dict;


def applyCoder(text, coder):
    """
    Applies the coder to the text. Returns the encoded text.

    text: string
    coder: dict with mappings of characters to shifted characters
    returns: text after mapping coder chars to original text
    """
    cypher = '';
    for char in text:
        if (char in string.digits or char in string.punctuation or char == ' ' or char == '\n'):
            cypher += char;
        else:
            cypher += coder.get(char);
    return cypher;


def applyShift(text, shift):
    """
    Given a text, returns a new text Caesar shifted by the given shift
    offset. Lower case letters should remain lower case, upper case
    letters should remain upper case, and all other punctuation should
    stay as it is.

    text: string to apply the shift to
    shift: amount to shift the text (0 <= int < 26)
    returns: text after being shifted by specified amount.
    """
    ### HINT: This is a wrapper function.
    return applyCoder(text, buildCoder(shift));

#
# Problem 2: Decryption
#
def findBestShift(wordList, text):
    """
    Finds a shift key that can decrypt the encoded text.

    text: string
    returns: 0 <= int < 26
    """
    highestNumberOfValidWordsFound = 0;
    keyWithHighestNumberOfValidWords = 0;
    for k in range(1, 26):
        decryptedMessage = applyShift(text, k);
        decryptedWords = decryptedMessage.split(' ');
        validWordCount = 0;
        for word in decryptedWords:
            if isWord(wordList, word):
                validWordCount += 1;
        if validWordCount > highestNumberOfValidWordsFound:
            highestNumberOfValidWordsFound = validWordCount
            keyWithHighestNumberOfValidWords = k
    return keyWithHighestNumberOfValidWords;


def decryptStory():
    """
    Using the methods you created in this problem set,
    decrypt the story given by the function getStoryString().
    Use the functions getStoryString and loadWords to get the
    raw data you need.

    returns: string - story in plain text
    """
    encryptedStory = getStoryString();
    wordList = loadWords();
    bestShift = findBestShift(wordList, encryptedStory);
    decryptedStory = applyShift(encryptedStory, bestShift);
    return decryptedStory;


#
# Build data structures used for entire session and run encryption
#

if __name__ == '__main__':
    #print(buildCoder(9));
    
    #print(applyCoder("Hello, world!", buildCoder(3)));
    #print(applyCoder("Khoor, zruog!", buildCoder(23)));
    
    #print(applyShift('This is a test.', 8));
    #print(applyShift('Bpqa qa i bmab.', 18))
    
    #s = applyShift('Hello, world!', 8)
    #print(s);
    #bestShift = findBestShift(wordList, s)
    #print(bestShift);
    #t = applyShift(s, 18);
    #print(t);
    
    # To test findBestShift:
    #wordList = loadWords()
    #s = applyShift('Hello, world!', 8)
    #bestShift = findBestShift(wordList, s)
    #assert applyShift(s, bestShift) == 'Hello, world!'
    # To test decryptStory, comment the above four lines and uncomment this line:
    print(decryptStory());