#L12 PROBLEM 5  (5 points possible)
#Write a generator, genPrimes, that returns the sequence of prime numbers on successive calls to its next() method: 2, 3, 5, 7, 11, ...
#
#Hints
#Ideas about the problem
#Have the generator keep a list of the primes it's generated.
# A candidate number x is prime if (x % p) != 0 for all earlier primes p.

def genPrimes():
    primeCandidate = 2;
    previousPrimes = []
    while True:
        if (len(previousPrimes) == 0):
            isPrime = True;
        else:            
            primeCandidate = primeCandidate + 1;
            isPrime = True;
            for prevPrime in previousPrimes:
                if ((primeCandidate % prevPrime)) == 0:
                    isPrime = False;
                    break;
        if (isPrime):
            yield primeCandidate;
            previousPrimes.append(primeCandidate);
            

thePrime = genPrimes();
print(thePrime.next());
print(thePrime.next());
print(thePrime.next());
print(thePrime.next());
print(thePrime.next());
print(thePrime.next());